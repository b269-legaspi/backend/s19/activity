console.log("Hello world");

const user = prompt("Input user").toLowerCase();
const password = prompt("Input password").toLowerCase();
const role = prompt("Input role").toLowerCase();

if (!user || !password || !role) {
  alert("Fields should not be empty");
} else
  switch (role) {
    case "admin":
      alert("Welcome back to the class portal, admin");
      break;
    case "teacher":
      alert("Thank you for logging in, teacher!");
      break;
    case "student":
      alert("Welcome to class portal, student!");
      break;
    default:
      alert("Role out of range");
      break;
  }

const averageFunction = function (num1, num2, num3, num4) {
  const average = Math.round(((num1 + num2 + num3 + num4) / 4) * 100) / 100;
  console.log(average);
  if (average <= 74) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is F."
    );
  } else if (average >= 75 && average <= 79) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is D."
    );
  } else if (average >= 80 && average <= 84) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is C."
    );
  } else if (average >= 85 && average <= 89) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is B."
    );
  } else if (average >= 90 && average <= 95) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is A."
    );
  } else if (average >= 96) {
    console.log(
      "Hello student, your average is " +
        average +
        ". The letter equivalent is A+."
    );
  }
};

averageFunction(75, 74, 75, 73.9);
averageFunction(79, 78, 78, 78);
averageFunction(84, 84, 83, 83);
averageFunction(89, 89, 89, 89);
averageFunction(94, 94, 94, 95);
averageFunction(97, 97, 97, 97);

// // [SECTION] If-else Statement
// let first_number = 0;
// let second_number = 11;

// // if(first_number < 10) {
// // 	console.log('Hello');
// // }

// // The 'if' statement checks for the initial condition. If the condition is true, then it will execute the code within the curly braces. But if the condition is false, then it will move on to an 'else if' statement and skip the first one entirely.
// // if(first_number > 0) {
// // 	console.log('Hello');
// // } else if (second_number == 10) {
// // 	console.log('World');
// // }

// if (first_number > 0) {
//   console.log("Hello");
// } else if (second_number == 10) {
//   console.log("World");
// } else {
//   console.log("Wala talaga eh");
// }

// let message = "No message";
// console.log(message);

// // You can use 'if-else' statements inside functions by using the function's parameter (wind_speed) within the condition of the if-else statements.
// function determineTyphoonIntensity(wind_speed) {
//   if (wind_speed < 30) {
//     return "Not a typhoon yet";
//   } else if (wind_speed <= 61) {
//     return "Tropical depression detected";
//   } else if (wind_speed >= 62 && wind_speed <= 88) {
//     return "Tropical Storm detected";
//   } else {
//     return "Typhoon detected!";
//   }
// }

// message = determineTyphoonIntensity(69);
// console.log(message);

// // Ternary Operator
// // This operator does the same thing as the regular 'if-else' statement. But it can only be used when the condition is not that long.
// // The parenthesis denotes the condition, the question mark denotes the end of the condition, and the colon denotes an 'else' statement.
// let result = 1 < 18 ? true : false;

// console.log("Result of the operator: " + result);

// // [SECTION] Switch Statement

// // let day = prompt('What day of the week is it today?').toLowerCase();

// // To check the initial value
// // console.log(day);

// // Switch statement checks the value of a variable and matches it with a certain 'case'. If none of the cases match, then it will go to the default keyword.
// // switch(day){
// // 	case 'monday':
// // 		console.log('The color of the day is red');
// // 		break;
// // 	case 'tuesday':
// // 		console.log('The color of the day is orange');
// // 		break;
// // 	case 'wednesday':
// // 		console.log('The color of the day is pink');
// // 		break;
// // 	case 'thursday':
// // 		console.log('The color of the day is green');
// // 		break;
// // 	case 'friday':
// // 		console.log('The color of the day is blue');
// // 		break;
// // 	case 'saturday':
// // 		console.log('The color of the day is indigo');
// // 		break;
// // 	case 'sunday':
// // 		console.log('The color of the day is violet');
// // 		break;
// // 	default:
// // 		console.log('Please input a valid day');
// // 		break;
// // }

// // [SECTION] Try-Catch-Finally Statement

// // Try block will execute a block of code and check if there is any errors. If any errors were detected, it will pass that data to the catch block and the catch block will handle that error accordingly. The 'finally' block is optional and will always run regardless of the result of the try-catch blocks.
// function showIntensityAlert(wind_speed) {
//   try {
//     alert(determineTyphoonIntensity(wind_speed));
//   } catch (error) {
//     console.log(error.message);
//   } finally {
//     alert("Intensity updates will show alert");
//   }
// }

// showIntensityAlert(56);
